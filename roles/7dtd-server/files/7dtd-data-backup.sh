#!/usr/bin/env bash
#
# Performs a quick, simple, tar+bzip backup of the
# 7 Days to Die game files on the server into a
# directory for easy consumption/restoration.
#
# Author: Thomas Farvour <tom@farvour.com>

set -e

BACKUP_SRC_DIR="$HOME/7dtd-data"
BACKUP_DEST_DIR="/mnt/backup/zed/sf_7DaysToDieDataBackups"
START_DATE=$(date -Iseconds)
TAR_CMD=$(which tar)
CHECKSUM_CMD=$(which md5deep)

# try to create the directory
if [ ! -d ${BACKUP_DEST_DIR} ]
then
  mkdir -p ${BACKUP_DEST_DIR}
  [ $? -eq 0 ] || (echo "Unable to create backup destination directory!"; exit 1)
fi

echo "------------------------------------------"
echo "Backup script started at: ${START_DATE}"

backup_filename_date=$(echo ${START_DATE} | tr ':' '-') # keeps filenames platform compatible.
backup_file="${BACKUP_DEST_DIR}/7DaysToDieData-${backup_filename_date}.tar.bz2"
backup_checksum_file="${backup_file}.md5sums"

echo "Backup source directory: ${BACKUP_SRC_DIR}"
echo "Backup destination directory: ${BACKUP_DEST_DIR}"

# compute a simple (albeit poor) sha256 of the entire source dir
# to see if things changed before bothering.
previous_backup_checksum_file=$(find ${BACKUP_DEST_DIR} -iname "*.md5sums" -print | sort | tail -n 1)

echo "Previous backup checksum file: ${previous_backup_checksum_file}"
echo

if [ -f "${previous_backup_checksum_file}" ]
then
  echo "A previous backup checksum was located, computing differences ..."
  mismatching_checksum_count=$(${CHECKSUM_CMD} -r -x ${previous_backup_checksum_file} ${BACKUP_SRC_DIR} | wc -l)
else
  mismatching_checksum_count="-1"
fi

if [[ "${mismatching_checksum_count}" == "0" ]]
then
  echo "Previous backup file checksums match current; skipping."
else
  echo "Backing up now ..."

  ${TAR_CMD} -cjvf ${backup_file} ${BACKUP_SRC_DIR}

  echo
  echo "Writing checksums ..."
  ${CHECKSUM_CMD} -r ${BACKUP_SRC_DIR} > ${backup_checksum_file}
  echo "Current backup checksum file: ${backup_checksum_file}"
fi

echo "Done!"
echo "Script ended at: $(date -Iseconds)"
echo "------------------------------------------"
